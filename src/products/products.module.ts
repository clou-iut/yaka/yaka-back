import { Module } from '@nestjs/common';
import { DessertsController } from './controllers/desserts.controller';
import { DrinkController } from './controllers/drinks.controller';
import { IngredientController } from './controllers/ingredients.controller';
import { DessertsService } from './services/desserts.service';
import { DrinksService } from './services/drinks.service';
import { IngredientsService } from './services/ingredients.service';
import { SauceController } from './controllers/sauces.controller';
import { SaucesService } from './services/sauces.service';
import { PlatesTypesService } from './services/platesTypes.service';
import { PlateTypeController } from './controllers/platesTypes.controller';

@Module({
  imports: [ProductsModule],
  controllers: [
    DessertsController,
    IngredientController,
    DrinkController,
    SauceController,
    PlateTypeController,
  ],
  providers: [
    DessertsService,
    IngredientsService,
    DrinksService,
    SaucesService,
    PlatesTypesService,
  ],
})
export class ProductsModule {}
