import { Injectable } from '@nestjs/common';
import { Ingredient } from '../models/ingredient.entity';
import { Dessert } from '../models/dessert.entity';
import { BaseEntity, ObjectType } from 'typeorm';
import { Product } from '../models/product.entity';
import { Drink } from '../models/drink.entity';
import { Sauce } from '../models/sauce.entity';
import { PlateType } from '../models/plateType.entity';

export enum productsTypes {
  SAUCE = 'sauce',
  INGREDIENT = 'ingredient',
  DRINK = 'drink',
  DESSERT = 'dessert',
  PLATE_TYPE = 'plateType',
}

const nameToClassMapping = {
  ingredient: Ingredient,
  sauce: Sauce,
  drink: Drink,
  dessert: Dessert,
  plateType: PlateType,
};

export abstract class ProductsService<T extends Product> {
  EntityManager: typeof Product;
  constructor(type: productsTypes) {
    this.EntityManager = nameToClassMapping[type];
  }

  async getAll(): Promise<T[]> {
    return this.EntityManager.find() as Promise<T[]>;
  }

  async getById(id: number): Promise<T | false> {
    const result = this.EntityManager.findOne(id);

    if (!result) {
      return false;
    }
    return result as Promise<T | false>;
  }

  async save(product: T): Promise<T> {
    return product.save();
  }

  async delete(product: T): Promise<boolean> {
    return !!(await product.remove());
  }
}
