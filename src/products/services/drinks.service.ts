import { Injectable } from '@nestjs/common';
import { ProductsService, productsTypes } from './products.service';
import { Drink } from '../models/drink.entity';

@Injectable()
export class DrinksService extends ProductsService<Drink> {
  constructor() {
    super(productsTypes.DRINK);
  }
}
