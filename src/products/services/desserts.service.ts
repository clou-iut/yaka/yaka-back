import { Injectable } from '@nestjs/common';
import { Dessert } from '../models/dessert.entity';
import { ProductsService, productsTypes } from './products.service';

@Injectable()
export class DessertsService extends ProductsService<Dessert> {
  constructor() {
    super(productsTypes.DESSERT);
  }
}
