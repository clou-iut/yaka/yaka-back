import { Injectable } from '@nestjs/common';
import { ProductsService, productsTypes } from './products.service';
import { Sauce } from '../models/sauce.entity';

@Injectable()
export class SaucesService extends ProductsService<Sauce> {
  constructor() {
    super(productsTypes.SAUCE);
  }
}
