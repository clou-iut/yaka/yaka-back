import { Injectable } from '@nestjs/common';
import { ProductsService, productsTypes } from './products.service';
import { PlateType } from '../models/plateType.entity';

@Injectable()
export class PlatesTypesService extends ProductsService<PlateType> {
  constructor() {
    super(productsTypes.PLATE_TYPE);
  }
}
