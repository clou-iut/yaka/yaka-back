import { Injectable } from '@nestjs/common';
import { Ingredient } from '../models/ingredient.entity';
import { ProductsService, productsTypes } from './products.service';

@Injectable()
export class IngredientsService extends ProductsService<Ingredient> {
  constructor() {
    super(productsTypes.INGREDIENT);
  }
}
