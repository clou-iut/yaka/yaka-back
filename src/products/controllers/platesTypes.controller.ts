import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  ParseIntPipe,
  UsePipes,
} from '@nestjs/common';
import { PlateType } from '../models/plateType.entity';
import { PlatesTypesService } from '../services/platesTypes.service';
import { ProductsController } from './products.controller';
import {
  ApiUseTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import {
  PartialValidationPipe,
  CompleteValidationPipe,
} from 'src/shared/validationPipes';
@ApiUseTags('products/platesTypes')
@Controller('products/platesTypes')
export class PlateTypeController extends ProductsController<PlateType> {
  constructor(private readonly platesTypeService: PlatesTypesService) {
    super(platesTypeService);
  }

  // Nest routing Decorators
  @Get()
  // OpenApi Decorators
  @ApiOkResponse({ type: PlateType })
  async getPlateTypes() {
    return this.getProducts();
  }

  // Nest routing Decorators
  @Get(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: PlateType })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async getPlateTypeById(@Param('id') id: number) {
    return this.getProductById(id);
  }

  // Nest routing Decorators
  @Post()
  // OpenApi Decorators
  @ApiCreatedResponse({ type: PlateType })
  // Nest pipes
  @UsePipes(CompleteValidationPipe)
  async addPlateType(@Body() plateType: PlateType) {
    return this.addProduct(plateType);
  }

  // Nest routing Decorators
  @Delete(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: PlateType })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async deletePlateTypeById(@Param('id') id: number) {
    return this.deleteProductById(id);
  }

  // Nest routing Decorators
  @Patch(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: PlateType })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  @UsePipes(PartialValidationPipe)
  async patchPlateTypeById(
    @Param('id') id: number,
    @Body() partialPlateType: PlateType,
  ) {
    return this.patchProductById(id, partialPlateType);
  }
}
