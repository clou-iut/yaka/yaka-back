import { Test } from '@nestjs/testing';
import { DessertsController } from './desserts.controller';
import { DessertsService } from '../services/desserts.service';
import { Dessert } from '../models/dessert.entity';

const dessertsFixtures = [new Dessert(), new Dessert()];

Object.assign(dessertsFixtures[0], {
  id: 1,
  name: 'Tagada',
  price: 80,
});

Object.assign(dessertsFixtures[1], {
  id: 2,
  name: 'Kinder Bueno',
  price: 80,
});

describe('CatsController', () => {
  let dessertsController: DessertsController;
  let dessertsService: DessertsService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [DessertsController],
      providers: [DessertsService],
    }).compile();

    dessertsService = module.get<DessertsService>(DessertsService);
    dessertsController = module.get<DessertsController>(DessertsController);
  });

  describe('getDesserts', () => {
    it('should return an array of desserts', async () => {
      const result = dessertsFixtures;
      jest
        .spyOn(dessertsService, 'getAll')
        .mockImplementation(() => Promise.resolve(result));

      // Check that the controller return the service response
      expect(await dessertsController.getDesserts()).toBe(result);
    });
  });

  describe('getDessertById', () => {
    it('should call dessert service getById with the right id and return the service response', async () => {
      const result = dessertsFixtures[1];
      const dessertId = result.id;

      const spy = jest
        .spyOn(dessertsService, 'getById')
        .mockImplementation(() => Promise.resolve(result));

      // Check that the controller return the service response
      expect(await dessertsController.getDessertById(dessertId)).toBe(result);

      // Check that the service was called only once
      expect(spy.mock.calls.length).toBe(1);

      // Check that the service was called with the right id
      expect(spy.mock.calls[0][0]).toBe(dessertId);
    });
  });

  describe('addDessert', () => {
    it('should call dessert service save with the dessert arg and return the service response', async () => {
      const result = dessertsFixtures[1];
      const spy = jest
        .spyOn(dessertsService, 'save')
        .mockImplementation(() => Promise.resolve(result));

      // Check that the controller return the service response
      expect(await dessertsController.addDessert(result)).toBe(result);

      // Check that the service was called only once
      expect(spy.mock.calls.length).toBe(1);

      // Check that the service was called with the right dessert
      expect(spy.mock.calls[0][0]).toBe(result);
    });
  });

  describe('deleteDessertById', () => {
    it('should call dessert service deleteById with the right id and return the service response', async () => {
      const result = dessertsFixtures[1];
      const dessertId = result.id;

      const spy1 = jest
        .spyOn(dessertsService, 'getById')
        .mockImplementation(() => Promise.resolve(result));
      const spy2 = jest
        .spyOn(dessertsService, 'delete')
        .mockImplementation(() => Promise.resolve(true));

      // Check that the controller return the service response
      expect(await dessertsController.deleteDessertById(dessertId)).toBe(
        result,
      );

      // Check that the service getById was called only once
      expect(spy1.mock.calls.length).toBe(1);

      // Check that the service getById was called with the right id
      expect(spy1.mock.calls[0][0]).toBe(dessertId);

      // Check that the service delete was called only once
      expect(spy2.mock.calls.length).toBe(1);

      // Check that the service delete was called with the right id
      expect(spy2.mock.calls[0][0]).toBe(result);
    });
  });

  describe('patchDessertById', () => {
    it('should call dessert service getById with the right id, apply changes, call service save and return the service response', async () => {
      const baseDessert = dessertsFixtures[1];
      const dessertId = baseDessert.id;

      const partialDessert = {
        name: 'new Tagada',
      };

      const result = Object.assign(baseDessert, partialDessert);

      const spy1 = jest
        .spyOn(dessertsService, 'getById')
        .mockImplementation(() => Promise.resolve(baseDessert));
      const spy2 = jest
        .spyOn(dessertsService, 'save')
        .mockImplementation(() => Promise.resolve(result));

      // Check that the controller return the service response
      expect(
        await dessertsController.patchDessertById(
          dessertId,
          partialDessert as Dessert,
        ),
      ).toBe(result);

      // Check that the service getById was called only once
      expect(spy1.mock.calls.length).toBe(1);

      // Check that the service getById was called with the right id
      expect(spy1.mock.calls[0][0]).toBe(dessertId);

      // Check that the service save was called only once
      expect(spy2.mock.calls.length).toBe(1);

      // Check that the service save was called with the modified dessert
      expect(spy2.mock.calls[0][0]).toBe(result);
    });
  });
});
