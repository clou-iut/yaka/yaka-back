import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  ParseIntPipe,
  UsePipes,
} from '@nestjs/common';
import { Ingredient } from '../models/ingredient.entity';
import { IngredientsService } from '../services/ingredients.service';
import { ProductsController } from './products.controller';
import {
  ApiUseTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import {
  PartialValidationPipe,
  CompleteValidationPipe,
} from 'src/shared/validationPipes';

@ApiUseTags('products/ingredients')
@Controller('products/ingredients')
export class IngredientController extends ProductsController<Ingredient> {
  constructor(private readonly ingredientsService: IngredientsService) {
    super(ingredientsService);
  }

  // Nest routing Decorators
  @Get()
  // OpenApi Decorators
  @ApiOkResponse({ type: Ingredient })
  async getIngredients() {
    return this.getProducts();
  }

  // Nest routing Decorators
  @Get(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Ingredient })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async getIngredientById(@Param('id') id: number) {
    return this.getProductById(id);
  }

  // Nest routing Decorators
  @Post()
  // OpenApi Decorators
  @ApiCreatedResponse({ type: Ingredient })
  // Nest pipes
  @UsePipes(CompleteValidationPipe)
  async addIngredient(@Body() ingredient: Ingredient) {
    return this.addProduct(ingredient);
  }

  // Nest routing Decorators
  @Delete(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Ingredient })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async deleteIngredientById(@Param('id') id: number) {
    return this.deleteProductById(id);
  }

  // Nest routing Decorators
  @Patch(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Ingredient })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  @UsePipes(PartialValidationPipe)
  async patchIngredientById(
    @Param('id') id: number,
    @Body() partialIngredient: Ingredient,
  ) {
    return this.patchProductById(id, partialIngredient);
  }
}
