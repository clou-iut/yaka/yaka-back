import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  ParseIntPipe,
  UsePipes,
} from '@nestjs/common';
import { Drink } from '../models/drink.entity';
import { DrinksService } from '../services/drinks.service';
import { ProductsController } from './products.controller';
import {
  ApiUseTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import {
  PartialValidationPipe,
  CompleteValidationPipe,
} from 'src/shared/validationPipes';

@ApiUseTags('products/drinks')
@Controller('products/drinks')
export class DrinkController extends ProductsController<Drink> {
  constructor(private readonly drinkService: DrinksService) {
    super(drinkService);
  }

  // Nest routing Decorators
  @Get()
  // OpenApi Decorators
  @ApiOkResponse({ type: Drink })
  async getDrinks() {
    return this.getProducts();
  }

  // Nest routing Decorators
  @Get(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Drink })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async getDrinkById(@Param('id') id: number) {
    return this.getProductById(id);
  }

  // Nest routing Decorators
  @Post()
  // OpenApi Decorators
  @ApiCreatedResponse({ type: Drink })
  // Nest pipes
  @UsePipes(CompleteValidationPipe)
  async addDrink(@Body() drink: Drink) {
    return this.addProduct(drink);
  }

  // Nest routing Decorators
  @Delete(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Drink })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async deleteDrinkById(@Param('id') id: number) {
    return this.deleteProductById(id);
  }

  // Nest routing Decorators
  @Patch(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Drink })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  @UsePipes(PartialValidationPipe)
  async patchDrinkById(@Param('id') id: number, @Body() partialDrink: Drink) {
    return this.patchProductById(id, partialDrink);
  }
}
