import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UsePipes,
  ParseIntPipe,
} from '@nestjs/common';
import { Sauce } from '../models/sauce.entity';
import { SaucesService } from '../services/sauces.service';
import { ProductsController } from './products.controller';
import {
  ApiUseTags,
  ApiOkResponse,
  ApiNotFoundResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import {
  CompleteValidationPipe,
  PartialValidationPipe,
} from 'src/shared/validationPipes';

@ApiUseTags('products/sauces')
@Controller('products/sauces')
export class SauceController extends ProductsController<Sauce> {
  constructor(private readonly sauceService: SaucesService) {
    super(sauceService);
  }

  // Nest routing Decorators
  @Get()
  // OpenApi Decorators
  @ApiOkResponse({ type: Sauce })
  async getSauces() {
    return this.getProducts();
  }

  // Nest routing Decorators
  @Get(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Sauce })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async getSauceById(@Param('id') id: number) {
    return this.getProductById(id);
  }

  // Nest routing Decorators
  @Post()
  // OpenApi Decorators
  @ApiCreatedResponse({ type: Sauce })
  // Nest pipes
  @UsePipes(CompleteValidationPipe)
  async addSauce(@Body() sauce: Sauce) {
    return this.addProduct(sauce);
  }

  // Nest routing Decorators
  @Delete(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Sauce })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async deleteSauceById(@Param('id') id: number) {
    return this.deleteProductById(id);
  }

  // Nest routing Decorators
  @Patch(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Sauce })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  @UsePipes(PartialValidationPipe)
  async patchSauceById(@Param('id') id: number, @Body() partialSauce: Sauce) {
    return this.patchProductById(id, partialSauce);
  }
}
