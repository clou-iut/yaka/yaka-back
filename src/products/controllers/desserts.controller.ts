import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UsePipes,
  ParseIntPipe,
} from '@nestjs/common';
import { Dessert } from '../models/dessert.entity';
import { DessertsService } from '../services/desserts.service';
import { ProductsController } from './products.controller';
import {
  ApiUseTags,
  ApiNotFoundResponse,
  ApiInternalServerErrorResponse,
  ApiCreatedResponse,
  ApiOkResponse,
} from '@nestjs/swagger';
import {
  PartialValidationPipe,
  CompleteValidationPipe,
} from '../../shared/validationPipes';
@ApiUseTags('products/desserts')
@Controller('products/desserts')
export class DessertsController extends ProductsController<Dessert> {
  constructor(private readonly dessertsService: DessertsService) {
    super(dessertsService);
  }

  // Nest routing Decorators
  @Get()
  // OpenApi Decorators
  @ApiOkResponse({ type: [Dessert] })
  async getDesserts() {
    return this.getProducts();
  }

  // Nest routing Decorators
  @Get(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Dessert })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async getDessertById(@Param('id') id: number) {
    return this.getProductById(id);
  }

  // Nest routing Decorators
  @Post()
  // OpenApi Decorators
  @ApiCreatedResponse({ type: Dessert })
  // Nest pipes
  @UsePipes(CompleteValidationPipe)
  async addDessert(@Body() dessert: Dessert) {
    return this.addProduct(dessert);
  }

  // Nest routing Decorators
  @Delete(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Dessert })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  async deleteDessertById(@Param('id') id: number) {
    return this.deleteProductById(id);
  }

  // Nest routing Decorators
  @Patch(':id')
  // OpenApi Decorators
  @ApiOkResponse({ type: Dessert })
  @ApiNotFoundResponse({ description: 'Not Found.' })
  // Nest pipes
  @UsePipes(new ParseIntPipe())
  @UsePipes(PartialValidationPipe)
  async patchDessertById(
    @Param('id') id: number,
    @Body() partialDessert: Dessert,
  ) {
    return this.patchProductById(id, partialDessert);
  }
}
