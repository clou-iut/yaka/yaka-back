import {
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Product } from '../models/product.entity';
import { ProductsService } from '../services/products.service';
export abstract class ProductsController<T extends Product> {
  productsService: ProductsService<T>;

  constructor(productsService: ProductsService<T>) {
    this.productsService = productsService;
  }

  async getProducts() {
    return this.productsService.getAll();
  }

  async getProductById(id: number) {
    const found = this.getProductByIdOr404(id);
    return found;
  }

  async addProduct(dessert: T) {
    return this.productsService.save(dessert);
  }

  async deleteProductById(id: number) {
    const found = await this.getProductByIdOr404(id);

    const result = this.productsService.delete(found);
    if (!result) {
      throw new InternalServerErrorException();
    }

    return found;
  }

  async patchProductById(id: number, partialProduct: Partial<T>) {
    const found = await this.getProductByIdOr404(id);

    delete partialProduct.id;

    const newProduct = Object.assign(found, partialProduct);
    return this.productsService.save(newProduct);
  }

  async getProductByIdOr404(id: number) {
    const found = await this.productsService.getById(id);

    if (!found) {
      throw new NotFoundException();
    }

    return found;
  }
}
