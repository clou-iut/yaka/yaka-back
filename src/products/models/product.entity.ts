import {
  Column,
  PrimaryGeneratedColumn,
  BaseEntity,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';
import { Length, IsNumber, IsPositive, Allow } from 'class-validator';

export abstract class Product extends BaseEntity {
  // OpenApi Decorators
  @ApiModelProperty({ readOnly: true, example: 1 })
  // Class-validator Decorators
  @IsNumber({}, { groups: ['GET'] })
  // TypeOrm Decorators
  @PrimaryGeneratedColumn()
  id: number;

  // OpenApi Decorators
  @ApiModelProperty()
  // Class-validator Decorators
  @Length(3, 20, { always: true })
  // TypeOrm Decorators
  @Column()
  name: string;

  // OpenApi Decorators
  @ApiModelProperty({ example: 80 })
  // Class-validator Decorators
  @IsPositive({ always: true })
  // TypeOrm Decorators
  @Column('int')
  price: number;

  // OpenApi Decorators
  @ApiModelProperty({ readOnly: true, example: '2019-11-22T19:36:13.846Z' })
  // TypeOrm Decorators
  @CreateDateColumn()
  createDate: Date;

  // OpenApi Decorators
  @ApiModelProperty({ readOnly: true, example: '2019-11-22T19:36:13.846Z' })
  // TypeOrm Decorators
  @UpdateDateColumn()
  updateDate: Date;
}
