import { Entity } from 'typeorm';
import { Product } from './product.entity';

@Entity()
export class Drink extends Product {}
