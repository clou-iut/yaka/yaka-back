import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from 'typeorm';
import { Product } from './product.entity';

@Entity()
export class Dessert extends Product {}
