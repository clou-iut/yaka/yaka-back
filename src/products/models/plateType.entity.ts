import { Entity, Column } from 'typeorm';
import { Product } from './product.entity';
import { ApiModelProperty } from '@nestjs/swagger';

@Entity()
export class PlateType extends Product {
  // OpenApi Decorators
  @ApiModelProperty({ example: 3 })
  // TypeOrm Decorators
  @Column('int')
  maxIngredients: number;

  // OpenApi Decorators
  @ApiModelProperty({ example: 2 })
  // TypeOrm Decorators
  @Column('int')
  maxSauces: number;
}
