import { ValidationPipe, ValidationPipeOptions } from '@nestjs/common';

const sharedConfig: ValidationPipeOptions = {
  transform: true,
  whitelist: true,
};

export const CompleteValidationPipe = new ValidationPipe({
  ...sharedConfig,
  groups: ['POST'],
});
export const PartialValidationPipe = new ValidationPipe({
  ...sharedConfig,
  skipUndefinedProperties: true,
});
