import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getConnection } from 'typeorm';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from '../src/app.module';

const ormConfig: any = {
  type: 'postgres',
  host: 'localhost',
  port: '5432',
  username: 'postgres',
  password: 'changeme',
  database: 'test',
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: true,
};

describe('AppController (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    await getConnection().synchronize(true);

    app = moduleFixture.createNestApplication();

    await app.init();
  });

  afterEach(async () => {
    await getConnection().close();
  });

  it('/products/desserts (GET)', () => {
    return request(app.getHttpServer())
      .get('/products/desserts')
      .expect(200)
      .expect([]);
  });

  it('/products/desserts (POST)', () => {
    return request(app.getHttpServer())
      .post('/products/desserts')
      .send({ name: 'tagada', price: 80 })
      .expect(201);
  });
});
