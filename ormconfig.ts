module.exports = {
  type: 'postgres',
  host: process.env.TYPEORM_HOST || 'localhost',
  port: process.env.TYPEORM_PORT || 5432,
  username: process.env.TYPEORM_USERNAME || 'postgres',
  password: process.env.TYPEORM_PASSWORD || 'changeme',
  database: process.env.TYPEORM_DATABASE || 'postgres',
  entities: ['dist/**/*.entity{.ts,.js}'],
  synchronize: true,
};
